This file contains a list of practical exercises that can be used if one wants
to make sure that the applicant acquires some experience on specific parts of
Debian.  Feel free to submit more.

Please don't ask an applicant to perform ALL of them unless you really know
what you are doing.


Manpages
--------

MP1. Have you ever written a manpage? If so, please tell me which. If not,
     please run manpage-alert or take a look at
     https://qa.debian.org/man-pages.html and choose a package listed
     there. Then please write a manpage for it, submit it to the BTS and
     tell me the bug number for it.


NMU and QA
----------

NQ1. Please look at https://bugs.debian.org/release-critical/debian/all.html,
     choose a bug listed there (please try to find one older than a few
     days), try to create a patch to fix it and submit this patch to the
     BTS. If you can't fix the bug, you should document what you've found
     out about the bug in the BTS. Then prepare, if possible, a NMU and
     send me a pointer to your NMU patch.

     Do the same thing again for 2 other bugs with severity important
     or higher. If you couldn't fix a bug, you should send me
     the bug numbers of the bugs you tried to fix.


NQ2. Have you ever done an NMU? If so, please tell me which.
     If not, please:
      1. run rc-alert or wnpp-alert and choose a bug listed there.
         It does not need to be a difficult bug to fix, but it is a good
         idea to find a bug older than a few days;
      3. create a patch to fix it and submit it to the BTS;
      4. prepare an NMU with the patch, and send me the same diff
         that you would submit to the BTS as an intent to NMU.


NQ3. Have you ever done a QA upload? If so, please tell me which.
     If not, please:
      1. Run wnpp-alert and choose among the orphaned packages listed
         there.
      2. Prepare a QA upload for one of those packages, fixing a bug or
         removing some dust from the debian/ directory.
      3. Send me the package as you would upload it.


NQ4. list-repos is a package introduced in Debian in 2007. The last
     revision occurred in 2009, using Debian 5.0 Lenny, resulting in a
     package with an empty lintian report. Currently, the package is orphaned.
     Your mission is make a full update for this package. Please, consider
     the following instructions:
      1. Keep the package orphaned.
      2. The new revision has to be sent to the release "unstable".
      3. Try to solve all possible lintian tags (including pedantic,
         informational, etc.). However, it is also acceptable to skip
         one or two lintian tags if they are unsolvable (e.g. a specific
         problem that can only be fixed from upstream side, as a file
         called configure.in instead of configure.ac).
      4. All files in any old format as debian/copyright and debian/rules
         have to be migrated to current standards.
      5. Currently, there are no opened bugs for list-repos (except for
         bug #16345 that marks the package as orphaned).
      6. The only known URL to access this package is
         http://people.debian.org/~eriberto/list-repos_1.3.dsc
      7. DON'T publish your package at mentors.debian.net, Salsa, GitHub,
         in your website or in any other public space in the Internet. Send
         the source files attached to an email message to me. I will need
         the .dsc, the .orig.tar.gz (or similar) and the .debian.tar.xz (if
         you have one).

     Note: the list-repos isn't a real package already uploaded to Debian. The
     bug numbers used here and inside the package are purely fictional.


Maintainer scripts
------------------

MS1. Suppose that you maintain a package called "foo", and that from
     version 1.0 it wants the configuration in /etc/foo/foo.conf, while
     in older versions it was /etc/foo.conf.
     Please write the maintainer scripts for version 1.0 that will
     guarantee a smooth upgrade from any older version.

     If you've already done something like this, please don't do it
     again and just point me at it.

Note: be *very* careful with MS1 because it is much harder than it seems,
and definitely harder than it should be. Please see
https://wiki.debian.org/DpkgConffileHandling for what the whole problem
really amounts to.


MS2. Please get the source code of nodm (currently version 0.13 in
     unstable) and have a look into debian/nodm.{config,postinst,postrm}
     Can you tell what is going on there, and have a guess at the
     reasons for it?

Note: MS2 gives an opportunity to show both debconf and the importance of
preseeding when supporting deployment in special setups like embedded
systems.

